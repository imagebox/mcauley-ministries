// Utils
var fs        = require('fs');
var path      = require('path');
var log       = require('fancy-log');
var through2  = require('through2');

// Gulps
var gulp      = require('gulp');
var watch     = require('gulp-watch');
var concat    = require('gulp-concat');
var plumber   = require('gulp-plumber');
var inject    = require('gulp-inject');
var rename    = require('gulp-rename');
var clone     = require('gulp-clone');

// JS
var uglify    = require('gulp-uglify');

// SASS
var sass      = require('gulp-sass');
var postcss   = require('gulp-postcss');
var sassGlob  = require('gulp-sass-glob');

// CSS
var csso            = require('postcss-csso');
var autoprefixer    = require('autoprefixer');
var mqpacker        = require('css-mqpacker');
var discardComments = require('postcss-discard-comments');

// SVG
var svgstore  = require('gulp-svgstore');
var svgmin    = require('gulp-svgmin');

// BrowserSync
var browserSync = require('browser-sync').create();

// Packaging Theme
var zip       = require('gulp-zip');

// Manually set new date modified for file
// (Gulp4 is dumb and doesn't change it)
function update_file_timestamp() {
  return through2.obj( function ( file, enc, cb ) {
    var date = new Date();
    file.stat.atime = date;
    file.stat.mtime = date;
    cb( null, file );
  });
}



/**
 * Serve
 */

gulp.task('browser-sync', function () {

  /**
   * BrowserSync
   */

  browserSync.init({
    proxy: 'http://localhost:8080'
  });
});



/**
 * SASS
 * ----
 */

gulp.task('sass', function task_sass() {

  var processors = [
    // Autoprefixr uses settings from
    // 'browserslist' key in package.json
    autoprefixer(),
    mqpacker({
      sort: true
    }),
    discardComments()
  ];

  return gulp.src('./site/wp-content/themes/boxpress/assets/scss/*.scss')
    .pipe(plumber(function (error) {
      log.error( error.message );
      this.emit('end');
    }))
    .pipe(sassGlob({
      ignorePaths: [
        '**/__*.scss',
      ]
    }))
    .pipe(sass({
      includePaths: [
        './node_modules/breakpoint-sass/stylesheets'
      ],
      errLogToConsole: true,
      outputStyle: 'nested',
      precision: 12
    }))
    .pipe(postcss(processors))
    .pipe(update_file_timestamp())
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/css'));
});


/**
 * CSS
 * ---
 * Creates min file while preserving the original
 */

gulp.task('css:minify', function task_css_minify() {
  
  var processors = [
    csso({
      comments: false,
      // restructure: false,
    })
  ];

  return gulp.src([
    './site/wp-content/themes/boxpress/assets/css/*.css',
    '!./site/wp-content/themes/boxpress/assets/css/*.min.css'
  ])
    .pipe(clone())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.css';
    }))
    .pipe(postcss(processors))
    .pipe(update_file_timestamp())
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/css'))
    .pipe(browserSync.stream());
});



/**
 * SVG
 * ---
 * [https://github.com/w0rm/gulp-svgstore]
 * [https://www.npmjs.com/package/gulp-svgmin]
 *
 * Combines and Inlines SVGs below the opening body tag
 */

gulp.task('svg', function task_svg() {
  var svgs = gulp.src('./site/wp-content/themes/boxpress/assets/svg/**/*.svg')
    .pipe( svgmin( function (file) {
      return {
        plugins: [{
          removeViewBox: false
        }, {
          cleanupIDs: {
            minify: true
          }
        }]
      };
    }))
    .pipe( svgstore({ inlineSvg: true }));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp.src([
    './site/wp-content/themes/boxpress/inc/svg.php'
  ])
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(update_file_timestamp())
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/inc'));
});



/**
 * JS
 * --
 */

gulp.task('js', function task_js() {
  return gulp.src([
    './site/wp-content/themes/boxpress/assets/js/libs/*.js',
    './site/wp-content/themes/boxpress/assets/js/plugins/*.js',
    './site/wp-content/themes/boxpress/assets/js/*.js'
  ])
    .pipe(plumber())
    .pipe(concat('site.js'))
    .pipe(update_file_timestamp())
    .pipe(gulp.dest('./site/wp-content/themes/boxpress/assets/js/build'));
});

gulp.task('js:minify', function task_js_minify() {
  return gulp.src([
    './site/wp-content/themes/boxpress/assets/js/build/*.js',
    '!./site/wp-content/themes/boxpress/assets/js/build/*.min.js',
  ])
    .pipe(clone())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.basename += '.min';
      path.extname = '.js';
    }))
    .pipe(update_file_timestamp())
    .pipe(gulp.dest( './site/wp-content/themes/boxpress/assets/js/build' ))
    .pipe(browserSync.stream());
});



/**
 * Gulp
 * ----
 */

// Watch
gulp.task( 'watch', function task_watch() {

  // Watch js files
  watch([
    './site/wp-content/themes/boxpress/assets/js/**/*.js',
    '!./site/wp-content/themes/boxpress/assets/js/build/**/*.js',
  ]).on('change', gulp.series( 'js', 'js:minify' ));

  // Watch scss files
  watch([
    './site/wp-content/themes/boxpress/assets/scss/**/*.scss'
  ]).on('change', gulp.series( 'sass', 'css:minify' ));

  // Watch svg files
  watch([
    './site/wp-content/themes/boxpress/assets/svg/**/*.svg'
  ]).on('change', gulp.series( 'svg' ));
});

// Default Task
gulp.task('default', gulp.series( 'watch' ));

// Serve Task
gulp.task('serve', gulp.parallel( 'browser-sync', 'watch' ));
