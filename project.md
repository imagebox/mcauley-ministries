
# Content:
https://imagebox.gathercontent.com/item/2845339


# Blog Categories

- News Releases
  - Normal Article
  - Optional Link to PDF at end

- In The News
  - Displays in homepage feed
  - Short Description
  - Links out to external article



# GO Live

- Redirect Everything from sub-dir to new homepage
  - Managed by Eliance

- Give A Record to IT guy when we're ready to launch
  - 

mcauley-ministries

mcauleyministries.org
mcauley-ministries.imagebox.com