<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('inc/banners/locations-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="loc-search">
				<div class="wrap">
					<?php
						$my_search = new WP_Advanced_Search('my-form');
						$my_search->the_form();
					?>
					<div id="wpas-results" class="result"></div>
					<!-- <div id="wpas-results"></div> -->
					<!-- <div id="wpas-debug"></div> -->
					
				</div>
			</section>

			<!--
			<section class="acf-map">
				<?php
					$mapquery = array( 
						'numberposts' => -1,
						'post_type' => 'locations',
						'orderby' => 'meta_value',
						'order' => 'ASC',
						'posts_per_page' => -1,
					);
				?>
				<?php 
					$maploop = new WP_Query( $mapquery );
					while ( $maploop->have_posts() ) : $maploop->the_post();
					$location = get_field('map');
				?>
					<div id="location-<?php the_ID(); ?>" class="marker" data-icon="<?php bloginfo('template_directory');?>/assets/img/global/icons/location-pin.png" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
						<p><strong><?php the_title(); ?></strong></p>
						<span class="excerpt"><?php the_excerpt();?></span>
						<p><a href="<?php the_permalink();?>">Learn More</a></p>
					</div>
				<?php 
					endwhile; 
				?>
			</section>-->
			

			


			<section class="section">
				<div class="wrap">
					
					<div class="entry-content">


						


						<ul class="location-list" id="list-data">
							<?php
								$mapquery2 = array( 
									'numberposts' => -1,
									'post_type' => 'locations',
									'orderby' => 'meta_value',
									'order' => 'ASC',
									'posts_per_page' => -1,
								);
							?>
							<?php 
								$maploop2 = new WP_Query( $mapquery2 );
								while ( $maploop2->have_posts() ) : $maploop2->the_post();
								$location2 = get_field('map');

								$lat_val = get_post_meta( get_the_ID(), 'loc_lat', true );
								$lng_val = get_post_meta( get_the_ID(), 'loc_lng', true );

							?>
							
								<li id="location-<?php the_ID(); ?>" class="marker" data-icon="<?php bloginfo('template_directory');?>/assets/img/global/icons/location-pin.png" data-lat="<?php echo $location2['lat']; ?>" data-lng="<?php echo $location2['lng']; ?>">
									<p><strong><?php the_title(); ?></strong></p>
									<p>Lat: <?php echo $lat_val; ?></p>
									<p>Lng: <?php echo $lng_val; ?></p>
									<!-- <span class="excerpt"><?php the_excerpt();?></span> -->
									<a class="button" href="<?php the_permalink();?>">Learn More</a>
								</li>
							<?php 
								endwhile; 
							?>

						</ul>






					</div><!-- .entry-content -->

				</div><!--.wrap-->
			</section><!--.section-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
