jQuery(document).ready(function($) {


	$('.gallery').magnificPopup({
		delegate: '.gallery-item a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return '<h1>' + item.el.attr('title') + '</h1>';
			}

		}
	});





	
}); /* end doc ready */