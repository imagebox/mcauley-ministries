jQuery(document).ready(function($) {

	/****************************************
	* Mobile Navigation
	****************************************/

	// Mobile Navigation Toggle
	$(function () {
		$('.toggle-nav').click(function () {
			toggleNav();
		});
	});

	function toggleNav() {
		if ($('#site-wrap').hasClass('show-nav')) {
			$('#site-wrap').removeClass('show-nav');
		} else {
			$('#site-wrap').addClass('show-nav');
		}
		if ($('#site-wrap').hasClass('show-nav')) {
			$('#page').click(function () {
				$('#site-wrap').removeClass('show-nav');
			});
		}
	}

	// Mobile Contact Toggle
	$(function () {
		$('.toggle-contact').click(function () {
			toggleContact();
		});
	});

	function toggleContact() {
		if ($('#site-wrap').hasClass('show-contact')) {
			$('#site-wrap').removeClass('show-contact');
		} else {
			$('#site-wrap').addClass('show-contact');
		}
		if ($('#site-wrap').hasClass('show-contact')) {
			$('#page').click(function () {
				$('#site-wrap').removeClass('show-contact');
			});
		}
	}

	// Hide nav/contact if open and window is resized above 760
	$(window).resize(function () {
		if ($(window).width() > 760) {
			$('#site-wrap').removeClass('show-nav');
			$('#site-wrap').removeClass('show-contact');
		}
	});


	/**
	 * Mobile Nav Controls
	 */

	// Add dropdown buttons
	$('.mobile-nav > ul > li.menu-item-has-children').append('<a class="mobile-child-toggle is-closed" href="#"><div class="toggle-arrow"></div></a>');

	var $mobileToggles = $('.mobile-child-toggle');
	var $all_sub_navs = $mobileToggles.prev();

	// Toggle sub-menu
	$mobileToggles.on('click', function () {
		var $this = $(this);
		var $sub_nav = $this.prev();

		// Hide all
		$all_sub_navs.slideUp(500);
		$mobileToggles.removeClass('is-open').addClass('is-closed');

		// Show Current
		if ( ! $sub_nav.is( ':visible' )) {
			$sub_nav.slideDown(500);
			$this.removeClass('is-closed').addClass('is-open');
		}

		return false;
	});

});
