(function($) {

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function render_map( $el ) {

	// var 
	var $markers = $el.find('.marker');

	// vars
	var args = {
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: true,
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		styles: [ 

{ 

"elementType": "geometry.fill", 

"stylers": [ 

{ "color": "#dddddd" } 

] 

},{ 

"elementType": "geometry.stroke", 

"stylers": [ 

{ "color": "#eeeeee" }, 

{ "weight": 0.6 } 

] 

},{ 

"featureType": "water", 

"elementType": "geometry.fill", 

"stylers": [ 

{ "color": "#bbbbbb" } 

] 

},{ 

"featureType": "transit.station", 

"stylers": [ 

{ "visibility": "simplified" } 

] 

},{ 

"featureType": "poi", 

"stylers": [ 

{ "visibility": "off" } 

] 

},{

	  "elementType": "labels",
	  "stylers": [
		{ 
			"visibility": "on",
			"color": "#444444"
		}
	  ]
	}

], 
	};


	// create map	        	
	var map = new google.maps.Map( $el[0], args);

	// add a markers reference


	map.markers = [];

	// add markers
	index=0;
	$markers.each(function(){

		add_marker( $(this), map );
		index++;
	});

	// center map
	center_map( map );

}  



/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/ 

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var $listImage = $marker.attr('data-icon');
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon : $listImage
	});



	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{

		


	var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		}); 


			// for whenever the static list items are clicked

			liTag = $("ul#list-data").find("[data-lat='" + $marker.attr('data-lat') + "']");
			 // console.log(liTag);
			// show info window when marker is clicked
			$(liTag).click(function() {
				infowindow.setContent($marker.html());
				if($('.gm-style-iw').length) {
					$('.gm-style-iw').parent().hide();
					$('.active-location').removeClass('active-location');
				}
				infowindow.open(map, marker);
				$('.active-location').removeClass('active-location');
				$(this).addClass('active-location');
		
				
			});





			// for whenever pins are clicked

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent($marker.html());
				if($('.gm-style-iw').length) {
					$('.gm-style-iw').parent().hide();
					$('.active-location').removeClass('active-location');
				}
				infowindow.open(map, marker);
			});






			// close info window when map is clicked
			 google.maps.event.addListener(map, 'click', function(event) {
				if (infowindow) {
					infowindow.close(); 
					$('.active-location').removeClass('active-location');
				}
				
			}); 







	} 

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
		map.setCenter( bounds.getCenter() );
		map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}



/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$(document).ready(function(){

	$('.acf-map').each(function(){

		render_map( $(this) );

	});


});



$(document).ajaxComplete(function() {
	// $('.acf-map').each(function(){
	// 	render_map( $(this) );
	// });
	console.log('derp');
	setTimeout(function(){
		$('.acf-map').each(function(){
			render_map( $(this) );
		});
	}, 1000);
});


})(jQuery);