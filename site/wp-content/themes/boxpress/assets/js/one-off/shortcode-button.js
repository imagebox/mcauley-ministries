(function () {
  tinymce.PluginManager.add('boxpress_mce_button', function ( editor, url ) {
    editor.addButton('boxpress_mce_button', {
      icon: false,
      text: "Button",
      onclick: function () {

        /**
         * For all allowed types, reference:
         * [http://stackoverflow.com/questions/24871792/tinymce-api-v4-windowmanager-open-what-widgets-can-i-configure-for-the-body-op]
         */

        editor.windowManager.open({
          title: "Insert Button Shortcode",
          body: [{
            type: 'textbox',
            name: 'buttonTitle',
            label: 'Button Title',
            value: 'New Button'
          },
          {
            type: 'textbox',
            name: 'buttonLink',
            label: 'Button URL',
            value: '#'
          },
          {
            type: 'listbox',
            name: 'buttonTarget',
            label: 'Open Link In',
            values: [
              { text: 'Same Tab', value: '_self' },
              { text: 'New Tab', value: '_blank' },
            ],
            value : '_self'
          }],
          onsubmit: function (e) {
            editor.insertContent(
              '[button ' +
              'link="' + e.data.buttonLink + '" ' +
              'target="' + e.data.buttonTarget + '" ' +
              ']' +
              e.data.buttonTitle + 
              '[/button]'
            );
          }
        });
      }
    });
  });
})();
