jQuery(document).ready(function($) {
  'use strict';

/****************************************
* Global, Non-specific JS
****************************************/

	
	// Wrap iframes in div w/ flexible-container class to make responsive
	$("iframe").wrap("<div class='flexible-container'></div>");


	$('#wpas-results-inner').addClass('acf-map');

	$('.loc-search #wpas-results').addClass('acf-loc-results');


  /**
   * Carousel
   */
  
  $('.owl-carousel').owlCarousel({
    nav: true,
    items: 4,
    loop: true,
    responsive : {
      0 : {
        // nav: false,
        items: 1,
      },
      480 : {
        // nav: true,
        items: 2,
      },
      760 : {
        // nav: true,
        items: 3,
      },
      960 : {
        // nav: true,
        items: 4,
      }
    }
  });
	
}); /* end doc ready */
