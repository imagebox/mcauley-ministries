jQuery(document).ready(function($) {


	if ($(".mobile-head").css("display") == "none" ){	
		$(window).on("scroll", function () {
			if ( $(this).scrollTop() > 40 ) {
				$("#masthead").addClass("minify-header");
				// $("#content").css("margin-top", "150px");
				$(".woo-header").css("display", "none");
			} else {
				$("#masthead").removeClass("minify-header");
				// $("#content").css("margin-top", "190px");
				$(".woo-header").css("display", "block");
			}

		});
	}

	if ($(".mobile-head").css("display") == "block" ){	
		$("#masthead").removeClass("minify-header");
		if ($(this).scrollTop() > 40) {
			// $("#content").css("margin-top", "0px");
		}
	}

	$(window).resize(function(){
		if ($(".mobile-head").css("display") == "none" ){	
			// $("#content").css("margin-top", "190px");
			$(window).on("scroll", function () {
				if ($(this).scrollTop() > 40) {
					$("#masthead").addClass("minify-header");
					// $("#content").css("margin-top", "150px");
					$(".woo-header").css("display", "none");
				} else {
					$("#masthead").removeClass("minify-header");
					// $("#content").css("margin-top", "190px");
					$(".woo-header").css("display", "block");
				}

			});
		}
		if ($(".mobile-head").css("display") == "block" ){	
			$("#masthead").removeClass("minify-header");
			// $("#content").css("margin-top", "0px");
		}
	});

}); /* end doc ready */