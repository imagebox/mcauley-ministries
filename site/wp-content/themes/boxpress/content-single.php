<?php
/**
 * @package BoxPress
 */

$sub_heading    = get_field('external_post_sub_heading');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	  <?php if ( ! empty( $sub_heading )) : ?>
	    <h2>
	      <?php echo $sub_heading; ?>
	    </h2>
	  <?php endif; ?>

		<?php 
			if ( has_post_thumbnail() ) {?>
				<?php the_post_thumbnail('home_index_thumb');?>
			<?php } else { ?>
				<!-- no thumbnail -->
			<?php }
		?>

		<div class="entry-meta">
		  <h4 class="post-date">
		    <?php
		    	$date = get_the_date('M j, Y');
		    	echo date_month_formatter( $date );
		    ?>
		    <?php
		      // if ( is_home() || is_category() ) {
		        $categories_list = get_the_category_list( __( ', ', 'boxpress' ) );
		        if ( $categories_list && boxpress_categorized_blog() ) {
		          printf( ' | <span class="cat-links">' . __( '%1$s', 'boxpress' ) . '</span>', $categories_list );
		        }
		      // }
		    ?>
		  </h4>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'boxpress' ),
				'after'  => '</div>',
			) );
		?>

	<footer class="entry-footer">
		<?php include_once('inc/social-share.php'); ?>
		<?php boxpress_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
