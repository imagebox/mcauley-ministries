
			</div><!-- #content -->

			<footer id="colophon" class="site-footer" role="contentinfo">

				<?php
					$feature_logo_text = get_field('footer_feature_logo_text', 'option');
					$feature_logo = get_field('footer_feature_logo', 'option');
					$feature_logo_link_url = get_field('footer_feature_logo_link_url', 'option');
					$footer_button_text = get_field('footer_button_text', 'option');
					$footer_button_url = get_field('footer_button_url', 'option');
				?>

				<div class="primary-footer">
					<div class="wrap">

						<div class="footer-col footer-col-1">
							<div class="footer-branding">
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php bloginfo('template_directory');?>/assets/img/global/branding/mcauley-ministries-logo--reverse.png" alt="McAuley Ministries">
								</a>

								<?php if ( ! empty( $feature_logo_text )) : ?>
									<p><?php echo $feature_logo_text; ?></p>
								<?php endif; ?>


								<?php if ( ! empty( $feature_logo )) : ?>
									<?php if ( ! empty( $feature_logo_link_url )) : ?>
										<a href="<?php echo $feature_logo_link_url; ?>" target="_blank">
									<?php endif; ?>

										<img src="<?php echo $feature_logo['url']; ?>" alt="<?php echo $feature_logo['alt']; ?>">

									<?php if ( ! empty( $feature_logo_link_url )) : ?>
										</a>
									<?php endif; ?>
								<?php endif; ?>


							</div>
						</div>

						<div class="footer-col footer-col-2">
							<div class="address">
								<div itemscope itemtype="http://schema.org/LocalBusiness">
									<h3><span itemprop="name"><?php the_field('business_name','option');?></span></h3>
									<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
										<p><span itemprop="streetAddress"><?php the_field('street','option');?></span><br>
										<span itemprop="addressLocality"><?php the_field('city','option');?></span>, <span itemprop="addressRegion"><?php the_field('state','option');?></span> <span itemprop="postalCode"><?php the_field('zip','option');?></span></p>
									</div>
									<div class="footer-fax-tel-email">
										<p><span itemprop="telephone">CALL <a href="tel:<?php the_field('phone','option');?>"><?php the_field('phone','option');?></a></span><br>
										EMAIL <a class="email" href="mailto:<?php the_field('email','option');?>" itemprop="email"><?php the_field('email','option');?></a></p>
									</div>

									<?php if ( ! empty( $footer_button_text ) && ! empty( $footer_button_url )) : ?>
										<a class="button" href="<?php echo $footer_button_url; ?>">
											<?php echo $footer_button_text; ?>
										</a>
									<?php endif; ?>

								</div><!-- end schema.org -->
							</div><!--.address-->
						</div>

						<div class="footer-col footer-col-3">
							<div class="social-media">
							  <?php if ( get_field( 'facebook', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('facebook', 'option'); ?>" title="Facebook"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'twitter_username', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="http://twitter.com/<?php the_field('twitter_username', 'option'); ?>"  title="Twitter"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'linkedin', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('linkedin', 'option'); ?>" title="LinkedIn"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'youtube', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('youtube', 'option'); ?>" title="YouTube"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'instagram', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('instagram', 'option'); ?>" title="Instagram"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'google_plus', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('google_plus', 'option'); ?>" title="Google+"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-google-plus fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'pinterest', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('pinterest', 'option'); ?>" title="Pinterest"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pinterest-p fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'tumblr', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('tumblr', 'option'); ?>" title="Tumblr"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-tumblr fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'flickr', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('flickr', 'option'); ?>" title="Flickr"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-flickr fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'vimeo', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('vimeo', 'option'); ?>" title="Vimeo"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-vimeo fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>
							  <?php if ( get_field( 'yelp', 'option' )) : ?>
							    <a class="fa-stack fa-lg" target="_blank" href="<?php the_field('yelp', 'option'); ?>" title="Yelp"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-yelp fa-stack-1x fa-inverse"></i></a>
							  <?php endif; ?>

							</div>

							<div class="footer-nav">
								<ul>
									<?php
										wp_nav_menu( array(
											'theme_location' => 'footer',
											'items_wrap' => '%3$s',
											'container' => false,
										));
									?>
								</ul>
							</div><!--.footer-nav-->

							<div class="site-info">
								<div class="copyright">
									<p>&copy; <?php echo date("Y"); ?> <a href="https://www.pittsburghmercy.org/" target="_blank">Pittsburgh Mercy Health System</a></p>
									<p>Photos by E.A. Smith for CREW Productions LLC</p>
								</div><!-- .copyright -->
								<div class="imagebox">
									<p>Website by <a href="https://imagebox.com" target="_blank" title="Pittsburgh Web Design, Graphic Design &amp; Marketing">Imagebox</a></p>
								</div><!-- .imagebox -->
							</div><!-- .site-info -->
						</div>

					</div><!-- .wrap -->
				</div><!-- .primary-footer -->
			</footer><!-- #colophon -->
		</div><!-- #page -->

	</div><!-- #canvas -->
</div><!-- #site-wrap -->

<?php wp_footer(); ?>
<?php include_once('inc/footer-scripts.php'); ?>	

</body>
</html>
