<?php

/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */


if ( ! function_exists( 'boxpress_setup' ) ) :
function boxpress_setup() {
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support('post-thumbnails');
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Custom Thumbnail Sizes
	 * Include Name, Width, Height and whethor or not to crop (true/false).
	 * ex: add_image_size( 'home_thumbnail', 110, 110, true);
	 * ex: add_image_size( 'custom-size', 220, 220, array( 'left', 'top' ) ); // Hard crop left top
	*/

	add_image_size( 'home_slideshow', 1200, 600, true );
	add_image_size( 'home_blog_thumb', 400, 400, true );
  add_image_size( 'home_index_thumb', 860, 350, true );
  add_image_size( 'home_carousel_thumb', 223, 223, true );

}
endif; // boxpress_setup
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */
function boxpress_scripts() {
	
	/*
		Styles
	*/
  // Main screen styles
  $style_screen_path  = get_template_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_template_directory() . '/assets/css/style.min.css' );
  wp_enqueue_style( 'screen', $style_screen_path, array(), $style_screen_ver, 'screen' );

  // Main print styles
  $style_print_path   = get_template_directory_uri() . '/assets/css/print.min.css';
  $style_print_ver    = filemtime( get_template_directory() . '/assets/css/print.min.css' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );

	/* 
		Modernizr 
	*/
	wp_register_script('modernizr', get_stylesheet_directory_uri() . '/assets/js/dev/modernizr.js', array('jquery'), false, false);
	wp_enqueue_script('modernizr');


	/*
		Minifed/Uglified Scripts
	*/
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );


	/*
		Single Posts
	*/
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );


/** 
 * Security Headers
 */
function boxpress_security_headers() {
  // Enable HSTS
  header( 'Strict-Transport-Security: max-age=31536000; includeSubDomains; preload' );
  // Prevent MIME transforms
  header( 'X-Content-Type-Options: nosniff' );
  // Prevent unwanted iframes (clickjacking)
  header( 'X-Frame-Options: SAMEORIGIN' );
  // Fix insecure content (Already set using Really Simple SSL)
  // header( 'Content-Security-Policy: upgrade-insecure-requests' );
}
add_action( 'send_headers', 'boxpress_security_headers' );


/**
 * Defer Javascripts
 * Defer jQuery Parsing using the HTML5 defer property
 */
// if (!(is_admin() )) {
// 	function defer_parsing_of_js ( $url ) {
// 		if ( FALSE === strpos( $url, '.js' ) ) return $url;
// 		if ( strpos( $url, 'jquery.js' ) ) return $url;
// 		if ( strpos( $url, 'modernizr.js' ) ) return $url;
// 		// return "$url' defer ";
// 		return "$url' defer onload='";
// 	}
// 	add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
// }


/**
 * Remove Admin Menu Sections
 * --------------------------
 */

function remove_wp_admin_menus() {
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_wp_admin_menus' );


function remove_wp_menu_bar_items( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'comments' );
  $wp_admin_bar->remove_node( 'customize' );
}
add_action( 'admin_bar_menu', 'remove_wp_menu_bar_items', 999 );


/**
 * Custom Excerpt
 */
function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($excerpt) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/**
 * Custom Excerpt
 * --------------
 * Remove shortcodes and headings from excerpt content
 */

function boxpress_custom_excerpt( $excerpt = '' ) {
  $raw_excerpt = $excerpt;

  // Set the excerpt if empty
  if ( '' == $excerpt ) {
    $excerpt = get_the_content('');
    $excerpt = strip_shortcodes( $excerpt );
    $excerpt = apply_filters('the_content', $excerpt);
    $excerpt = str_replace(']]>', ']]&gt;', $excerpt);
  }

  $regex    = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
  $excerpt  = preg_replace( $regex,'', $excerpt );

  $excerpt_length = apply_filters( 'excerpt_length', 30 );
  $excerpt_more   = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
  $excerpt        = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );

  return apply_filters( 'wp_trim_excerpt', preg_replace( $regex,'', $excerpt ), $raw_excerpt );
}
add_filter( 'get_the_excerpt', 'boxpress_custom_excerpt', 9);



/**
 * Clean Shortcodes
 * ----------------
 * Remove `p` and `br` tags from any shortcodes
 * in content
 */

function boxpress_clean_shortcodes( $content ) {   
  
  // els to remove
  $array = array(
    '<p>['    => '[',
    ']</p>'   => ']',
    '<div>['  => '[',
    ']</div>' => ']',
    ']<br />' => ']',
    ']<br>'   => ']',
    '<br />[' => '[',
    '<br>['   => '[',
  );

  // Remove dem els
  $content = strtr( $content, $array );
  return $content;
}
add_filter('the_content', 'boxpress_clean_shortcodes');



/**
 * Custom Shortcodes
 * -----------------
 */

// Basic Button
// Usage: [button link="#" color="button--callout-3"]Text[/button]
function button_shortcode( $atts, $content = null ) {
  extract( shortcode_atts( array( 'link' => '#', 'target' => '_self' ), $atts ));

  return '<p><a class="button button-shortcode" href="' . $link . '" target="' . $target . '"><span>' . do_shortcode( $content ) . '</span></a></p>';
}

// Callout
function callout_shortcode( $atts, $content = null ) {
  return '<div class="content-callout">' . do_shortcode( $content ) . '</div>';
}

function register_shortcodes() {
  add_shortcode('button', 'button_shortcode');
  add_shortcode('callout', 'callout_shortcode');
}

add_action( 'init', 'register_shortcodes');



/**
 * Customize Archive Title
 */

add_filter( 'get_the_archive_title', function ( $title ) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  }
  return $title;
});



/**
 * Date Formatter
 * --------------
 * Takes date in the format of 'M j, Y'
 */

function date_month_formatter( $date ) {

  $formatted_date = $date;

  $test_string = substr( $date, 0, 3 );

  switch ($test_string) {
    case 'Jan':
      $formatted_date = str_replace( $test_string, "Jan.", $date );
      break;
    case 'Feb':
      $formatted_date = str_replace( $test_string, "Feb.", $date );
      break;
    case 'Mar':
      $formatted_date = str_replace( $test_string, "March", $date );
      break;
    case 'Apr':
      $formatted_date = str_replace( $test_string, "April", $date );
      break;
    case 'May':
      break;
    case 'Jun':
      $formatted_date = str_replace( $test_string, "June", $date );
      break;
    case 'Jul':
      $formatted_date = str_replace( $test_string, "July", $date );
      break;
    case 'Aug':
      $formatted_date = str_replace( $test_string, "Aug.", $date );
      break;
    case 'Sep':
      $formatted_date = str_replace( $test_string, "Sept.", $date );
      break;
    case 'Oct':
      $formatted_date = str_replace( $test_string, "Oct.", $date );
      break;
    case 'Nov':
      $formatted_date = str_replace( $test_string, "Nov.", $date );
      break;
    case 'Dec':
      $formatted_date = str_replace( $test_string, "Dec.", $date );
      break;
      
    default:
      break;
  }

  return $formatted_date;
}



/**
 * TinyMCE Shortcode Buttons
 * -------------------------
 */

// Filter Functions with Hooks
function boxpress_mce_button() {

  // Check if user have permission
  if ( ! current_user_can( 'edit_posts' ) &&
       ! current_user_can( 'edit_pages' )) {
    return;
  }

  // Check if WYSIWYG is enabled
  if ( 'true' == get_user_option( 'rich_editing' )) {
    add_filter( 'mce_external_plugins', 'boxpress_tinymce_plugin' );
    add_filter( 'mce_buttons', 'boxpress_register_mce_button' );

  }
}
add_action('admin_head', 'boxpress_mce_button');


// Function for new button
function boxpress_tinymce_plugin( $plugin_array ) {
  $plugin_array['boxpress_mce_button'] = get_template_directory_uri() . '/assets/js/one-off/shortcode-button.js?123123';
  return $plugin_array;
}


// Register new button in the editor
function boxpress_register_mce_button( $buttons ) {
  array_push( $buttons, 'boxpress_mce_button', 'boxpress_mce_pdf_button' );
  return $buttons;
}


// Check if page is direct child
function is_child( $page_id ) { 
  global $post;

  if ( is_page() && ( $post->post_parent == $page_id )) {
    return true;
  } else { 
    return false;
  }
}

// Check if page is an ancestor
function is_ancestor( $post_id ) {
  global $wp_query;
  $ancestors = $wp_query->post->ancestors;

  if ( ! empty( $post_id ) && ! empty( $ancestors )) {
    if ( in_array( $post_id, $ancestors )) {
      return true;
    } else {
      return false;
    }
  }
}



/**
 * Widgets
 * =======
 */


// Allow use of shortcodes

function html_widget_title( $title ) {
  //HTML tag opening/closing brackets
  
  $title = str_replace( '&#91;', '<', $title );
  $title = str_replace( '&#91;/', '</', $title );
  $title = str_replace( '[', '<', $title );
  $title = str_replace( '[/', '</', $title );

  //<strong></strong>
  $title = str_replace( 'strong]', 'strong>', $title );
  //<em></em>
  $title = str_replace( 'em]', 'em>', $title );
  //<br>
  $title = str_replace( 'br]', 'br>', $title );

  return $title;
}
add_filter( 'widget_title', 'html_widget_title' );



/**
 * Callout Widget
 * --------------
 */

class my_custom_widget extends WP_Widget {
  function __construct() {
    parent::__construct(
      'yiw_pro_widget',
      __('Callout Widget', 'wordpress'),
      array( 'description' => __( 'Creates a callout widget', 'wordpress' ), )
    );
  }


  public function widget( $args, $instance ) {
    $title      = apply_filters( 'widget_title', $instance['title'] );
    $link_title = apply_filters( 'widget_title', $instance['link_title'] );
    $link_url   = apply_filters( 'widget_title', $instance['link_url'] );

    echo $args['before_widget'];

    echo '<div class="sidebar-callout callout--letter color-option-5">
            <div class="sidebar-callout-container">
              <svg width="42" height="53">
                <use xlink:href="#icon--clock"></use>
              </svg>

              <h3>' . $title . '</h3>
              <a class="button" href="' . esc_url( $link_url ) . '" target="_blank">' . $link_title . '</a>
            </div>
          </div>';

    echo $args['after_widget'];
  }


  public function form( $instance ) {
    $title  = ( isset( $instance[ 'title' ] )) ? $instance[ 'title' ] : '';
    $link_title   = ( isset( $instance[ 'link_title' ] )) ? $instance[ 'link_title' ] : '';
    $link_url   = ( isset( $instance[ 'link_url' ] )) ? $instance[ 'link_url' ] : '';

    ?>
      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'link_title' ); ?>"><?php _e( 'Link Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link_title' ); ?>" name="<?php echo $this->get_field_name( 'link_title' ); ?>" type="text" value="<?php echo esc_attr( $link_title ); ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'link_url' ); ?>"><?php _e( 'Link URL:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
      </p>
    <?php
  }


  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['link_title'] = ( ! empty( $new_instance['link_title'] ) ) ? strip_tags( $new_instance['link_title'] ) : '';
    $instance['link_url'] = ( ! empty( $new_instance['link_url'] ) ) ? strip_tags( $new_instance['link_url'] ) : '';
    return $instance;
  }
}

function load_my_custom_widget() {
  register_widget( 'my_custom_widget' );
}
add_action( 'widgets_init', 'load_my_custom_widget' );





/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom Post Types
 */
// require get_template_directory() . '/inc/cpt/locations.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin/default.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * ACF Options Pages
 */
require get_template_directory() . '/inc/acf-options.php';

/**
 * WP Advanced Search
 * http://wpadvancedsearch.com/
 */
// require_once('wp-advanced-search/wpas.php');

/**
 * Forms built with WPAS
 */
// require get_template_directory() . '/inc/forms.php';

/**
 * WooCommerce Functions
 */
// require get_template_directory() . '/inc/woocommerce.php';




