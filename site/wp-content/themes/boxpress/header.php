<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,700,700italic,500italic' rel='stylesheet' type='text/css'>

	<?php wp_head(); ?>
	<?php // Google Analytics 
		if(get_field('tracking_code', 'option')) { ?>
		<?php the_field('tracking_code', 'option'); ?>
	<?php } ?>

	<?php if( have_rows('innerpage_master') ): while ( have_rows('innerpage_master') ) : the_row(); ?>
	

	<?php if( get_row_layout() == 'google_map' ): ?>

		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
		<script src="<?php bloginfo('template_directory');?>/assets/js/lib/acf-map.js"></script>

	<?php endif; endwhile; else : endif; ?>


	<?php if(get_field('map')) {
		echo '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>';
		echo '<script src="/wp-content/themes/boxpress/assets/js/one-off/acf-map.js"></script>';
	} ?>
</head>

<body <?php body_class(); ?>>

<?php include_once('inc/svg.php'); ?>
<?php // include_once('inc/browse-happy.php'); ?>	

<div id="site-wrap">
	<div id="canvas">

		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'boxpress' ); ?></a>

		<?php include_once('inc/header-mobile.php'); ?>	

		<div id="page" class="hfeed site <?php if ( is_page_template( 'page-home.php' ) ) { echo 'home'; } else { echo 'innerpage'; } ?>">

			<header id="masthead" class="site-header" role="banner">
				<div class="wrap header-top-wrap">

					<div class="site-branding">

						
						<!-- <h1 class="site-title"> -->
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					      <svg class="site-logo-svg" width="219" height="90" viewBox="0 0 219 90">
					        <use xlink:href="#mcauley-ministries-logo"></use>
					      </svg>
							</a>
						<!-- </h1> -->

					</div><!-- .site-branding -->

					<nav class="top-nav" role="navigation">
						<ul>
							<?php
								wp_nav_menu( array(
									'theme_location' => 'secondary',
									'items_wrap' => '%3$s',
									'container' => false
								));
							?>
							<li class="top-nav-search">
								<?php get_template_part('inc/search-bar'); ?>
							</li>
						</ul>
					</nav><!-- .top-nav -->

				</div><!-- .wrap -->
	
				<nav class="main-nav" role="navigation">
					<div class="wrap">
						<ul>
							<?php
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'items_wrap' => '%3$s',
									'container' => false
								));
							?>
						</ul>
					</div>
				</nav>

			</header><!-- #masthead -->


			<div id="content" class="site-content">
