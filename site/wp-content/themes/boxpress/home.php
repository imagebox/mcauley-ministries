<?php
/**
 * The template for displaying the blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('inc/banners/blog-banners.php');?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
      <section class="blog-page">
        <div class="wrap">
          
          <div class="entry-content">

            <header class="page-header">
              <h1 class="page-title">In the News &amp; News Releases</h1>
            </header><!-- .page-header -->

            <?php if ( have_posts() ) : ?>

              <?php /* Start the Loop */ ?>
              <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part('template-parts/post-preview'); ?>

              <?php endwhile; ?>

              <?php the_posts_navigation(); ?>

            <?php else : ?>

              <?php get_template_part( 'content', 'none' ); ?>

            <?php endif; ?>

          </div><!-- .entry-content -->

          <?php get_sidebar(); ?>

        </div><!--.wrap-->
      </section><!--.blog-page-->
    </main><!-- #main -->
  </div><!-- #primary -->

<?php get_footer(); ?>
