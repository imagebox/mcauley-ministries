<?php
/**
 * Displays the Blog Banner
 */

$featured_blog_image = get_field('option_blog_featured_image', 'option');
$default_banner_image = get_field('option_default_banner_image', 'option');
?>
<header class="banner split" role="banner">
  <div class="banner-container">
    <div class="title">
      <span class="h1">News</span>
    </div>
    <?php
      if ( ! empty( $featured_blog_image )) {

        $featured_blog_image_url = $featured_blog_image['url'];
        echo '<img src="' . $featured_blog_image_url . '" alt="">';

      } elseif ( ! empty( $default_banner_image )) {
        
        $default_banner_image_url = $default_banner_image['url'];
        echo '<img src="' . $default_banner_image_url . '" alt="">';

      } else {

        echo '<img src="' . get_bloginfo('template_directory') . '/assets/img/global/banners/interior-masthead.jpg" alt="">';
      }
    ?>
  </div>
</header><!-- .entry-header -->
