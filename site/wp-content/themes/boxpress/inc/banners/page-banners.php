<?php
/**
 * Displays the page banners
 */

$default_banner_image = get_field('option_default_banner_image', 'option');

if ( !is_front_page() ) : ?>

	<header class="banner split" role="banner">
		<div class="banner-container">
			<div class="title">
				<span class="h1">	
					<?php if ( 0 == $post->post_parent ) {
						the_title(); } else {
						$parents = get_post_ancestors( $post->ID );
						echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); } 
					?>
				</span>
			</div>
			<?php 
				if ( has_post_thumbnail() ) { 
					the_post_thumbnail();

	      } elseif ( ! empty( $default_banner_image )) {
	        
	        $default_banner_image_url = $default_banner_image['url'];
	        echo '<img src="' . $default_banner_image_url . '" alt="">';

	      } else {

	        echo '<img src="' . get_bloginfo('template_directory') . '/assets/img/global/banners/interior-masthead.jpg" alt="">';
	      }
			?>
		</div>
	</header><!-- .entry-header -->

<?php endif; ?>
