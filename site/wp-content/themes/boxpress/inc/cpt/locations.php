<?php 

/**
 * BoxPress Locations Custom Post Type
 *
 * @package BoxPress
 */


/**
 * Register Custom Post Type
 */

function cpt_locations() {

	$labels = array(
		'name'                  => _x( 'Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Locations', 'text_domain' ),
		'name_admin_bar'        => __( 'Location', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Location:', 'text_domain' ),
		'all_items'             => __( 'All Locations', 'text_domain' ),
		'add_new_item'          => __( 'Add New Location', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Location', 'text_domain' ),
		'edit_item'             => __( 'Edit Location', 'text_domain' ),
		'update_item'           => __( 'Update Location', 'text_domain' ),
		'view_item'             => __( 'View Location', 'text_domain' ),
		'search_items'          => __( 'Search Location', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'items_list'            => __( 'Locations list', 'text_domain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter locations list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Location', 'text_domain' ),
		'description'           => __( 'Location Custom Post Type with Map', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'location_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'locations', $args );

}
add_action( 'init', 'cpt_locations', 0 );



/**
 * Register Custom Taxonomy
 */
function taxonomy_location_categories() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Categories', 'text_domain' ),
		'all_items'                  => __( 'All Categories', 'text_domain' ),
		'parent_item'                => __( 'Parent Category', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Category:', 'text_domain' ),
		'new_item_name'              => __( 'New Category Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Category', 'text_domain' ),
		'edit_item'                  => __( 'Edit Category', 'text_domain' ),
		'update_item'                => __( 'Update Category', 'text_domain' ),
		'view_item'                  => __( 'View Category', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Categories', 'text_domain' ),
		'search_items'               => __( 'Search Categories', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'items_list'                 => __( 'Categories list', 'text_domain' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'location_categories', array( 'locations' ), $args );

}
add_action( 'init', 'taxonomy_location_categories', 0 );




/**
 * Save the lng and lat values into a seperate post meta values for each post on post update.
 * This might be useful in extending the functionality of the maps later.
 *
 * @see http://support.advancedcustomfields.com/forums/topic/google-map-latlon-data/
 */

function boxpress_update_latlon($post_id, $post, $update) {

	$map = get_post_meta($post_id, 'map', true);

	if (!empty($map)) {
	    update_post_meta( $post_id, 'loc_lat', $map['lat'] );
	    update_post_meta( $post_id, 'loc_lng', $map['lng'] );
	}

}
add_action('save_post', 'boxpress_update_latlon', 90, 3);



function my_search_form() {
    
    $args['form']['ajax'] = array(
    	'enabled' => true,
    	'auto_submit' => true,
    	'results_template' => 'map-results.php', // This file must exist in your theme root
		'show_default_results' => true
	);
    
    $args['wp_query'] = array(
    	'post_type' => 'locations',
		'posts_per_page' => -1
	);
    
    $args['fields'][] = array(
    	'type' => 'search',
		'title' => 'Search',
		'placeholder' => 'Enter search terms...'
	);
    
    $args['fields'][] = array(
    	'type' => 'taxonomy',
		'taxonomy' => 'location_categories',
		'format' => 'select',
		// 'operator' => 'AND',
		'allow_null' => 'Categories'
	);
    
    $args['fields'][] = array(
    	'type' => 'submit'
    );

    register_wpas_form('my-form', $args);    
}
add_action('init', 'my_search_form');








