<div class="mobile-head">
	<a href="#" class="icon-menu toggle-nav"><i class="fa fa-bars"></i><span class="screen-reader-text">Menu</span></a>
	<a href="#" class="icon-location toggle-contact"><i class="fa fa-map-marker"></i><span class="screen-reader-text">Contact/Location</span></a>
</div>

<nav id="mobile-nav" class="mobile-nav" role="navigation">
	<ul>
		<?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '%3$s', 'container' => false )); ?>
	</ul>
	<ul>
		<?php wp_nav_menu( array('theme_location' => 'secondary', 'items_wrap' => '%3$s', 'container' => false )); ?>
	</ul>
</nav>

<div id="mobile-contact" class="mobile-contact">
	<div class="address">
		<div>
			<h3><?php the_field('business_name','option');?></h3>
			<p><?php the_field('street','option');?><br />
			<?php the_field('city','option');?>, <?php the_field('state','option');?> <?php the_field('zip','option');?></p>
			
			<div class="footer-fax-tel-email">
				<p><a class="tel" href="tel:<?php the_field('phone','option');?>"><?php the_field('phone','option');?></a></span><br />
				<a class="email" href="mailto:<?php the_field('email','option');?>" ><?php the_field('email','option');?></a></p>
			</div>
		</div>

	</div><!--.address-->
</div>