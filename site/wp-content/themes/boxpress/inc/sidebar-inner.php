
<div class="sidebar">


	<?php
		global $wp_query;
		$post 			= $wp_query->post;
		$ancestors 	= get_post_ancestors($post);
		
		if ( empty( $post->post_parent )) {
	    $parent = $post->ID;
		} else {
	    $parent = end($ancestors);
		}
	?>

	<?php if ( wp_list_pages( "title_li=&child_of=$parent&echo=0" )) : ?>

		<aside class="sidebar-nav">
			<ul>
				
				<?php
					wp_list_pages( array(
					  'title_li' 	=> '',
					  'child_of' 	=> $parent,
					  'depth' 		=> 4,
					));
				?>

			</ul>
		</aside><!-- /.sidebar-nav -->

	<?php endif; ?>

	<?php // Only display on 'How to Apply' pages ?>
	<?php if ( is_page(184) || is_ancestor(184) ) : ?>
		
		<div class="sidebar-callout color-option-4">
			<div class="sidebar-callout-container">
				<h2>Grant Application Dates</h2>

				<?php
					$major_grants_title = get_field( 'major_grants_title', 'option' );
				?>
				<?php if ( ! empty($major_grants_title) ) : ?>
					<h3><?php echo $major_grants_title; ?></h3>
				<?php endif; ?>

				<?php if ( have_rows('major_grants', 'option') ) : ?>
					<ul class="grants-list">
						<?php while ( have_rows('major_grants', 'option') ) : the_row(); ?>
							<?php
	              $date 						= get_sub_field('major_grant_date');
	              $is_closed 				= get_sub_field('is_major_grant_closed');
	              $grant_close_date = get_sub_field( 'major_grant_date' );

	              // Check dates
	              $now = wp_date( 'M j, Y' );
	              $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
	              $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
							?>
							<li>
								<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
									<h4>Closed</h4>
								<?php else : ?>
									<h4><?php echo date_month_formatter( $date ); ?></h4>
								<?php endif; ?>
								<?php the_sub_field('major_grant_title'); ?>
							</li>

						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<hr>

				<?php
					$outreach_grants_title = get_field( 'outreach_grants_title', 'option' );
				?>
				<?php if ( ! empty($outreach_grants_title) ) : ?>
					<h3><?php echo $outreach_grants_title; ?></h3>
				<?php endif; ?>

				<?php if ( have_rows('outreach_grants', 'option') ) : ?>
					<ul class="grants-list">
						<?php while ( have_rows('outreach_grants', 'option') ) : the_row(); ?>
							<?php
                $date 						= get_sub_field('outreach_grant_date');
                $is_closed 				= get_sub_field('is_outreach_grant_closed');
                $grant_close_date = get_sub_field( 'outreach_grant_date' );

                // Check dates
                $now = wp_date( 'M j, Y' );
                $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
                $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
							?>
							<li>
								<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
									<h4>Closed</h4>
								<?php else : ?>
									<h4><?php echo date_month_formatter( $date ); ?></h4>
								<?php endif; ?>
								<?php the_sub_field('outreach_grant_grant_title'); ?>
							</li>

						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<a href="#" class="button">Apply For A Grant</a>
			</div>
		</div>

	<div class="sidebar-callout callout--letter color-option-5">
		<div class="sidebar-callout-container">
      <svg width="42" height="53">
        <use xlink:href="#icon--clock"></use>
      </svg>

			<h3>Letter Of Inquiry is Due <strong>Febuary 3, 2017</strong> For Major Grants</h3>
			<a class="button" href="#">Submit Letter</a>
		</div>
	</div>

	<?php endif; ?>

</div>
