<div class="sharing">
	Share on: 
	<a class="js-social-share fa-stack fa-lg" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode('' . the_permalink() . ''); ?>">
		<i class="fa fa-circle fa-stack-2x"></i>
		<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
	</a>
	<a class="js-social-share fa-stack fa-lg" target="_blank" href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php echo urlencode('' . the_permalink() . ''); ?>&via=<?php the_field('twitter_username', 'option'); ?>">
		<i class="fa fa-circle fa-stack-2x"></i>
		<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
	</a>
	<a class="js-social-share fa-stack fa-lg" target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode('' . the_permalink() . ''); ?>">
		<i class="fa fa-circle fa-stack-2x"></i>
		<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
	</a>
	<a class="js-social-share fa-stack fa-lg" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('' . the_permalink() . ''); ?>&title=<?php the_title(); ?>&source=<?php echo urlencode('' . the_permalink() . ''); ?>&summary=<?php the_excerpt(); ?>">
		<i class="fa fa-circle fa-stack-2x"></i>
		<i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
	</a>
</div>