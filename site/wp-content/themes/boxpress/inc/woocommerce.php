<?php

/**
 * BoxPress WooCommerce functions and definitions
 *
 * @package BoxPress
 */


/**
 * Declare Support
 */

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}