<div class="woo-header">
	<div class="wrap">
		<?php 
			global $current_user;
      		get_currentuserinfo();
      	?>	
		<?php if ( is_user_logged_in() ) { ?>
			<div class="welcome">
				<p><?php _e('Welcome back','woothemes'); ?>, <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php echo $current_user->user_firstname; ?></a>!</p>
			</div><!--.welcome-->
		<?php } ?>
		<ul>
			<?php if ( is_user_logged_in() ) { ?>
				<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></li>
			<?php } else { ?>
				<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login / Register','woothemes'); ?></a></li>
			<?php } ?>
				<li><a title="<?php _e( 'View your shopping cart' ); ?>" href="<?php echo WC()->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"></i> Cart (<?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?>)</a></li>
			<?php
				if ( is_user_logged_in() ) {
					echo '<li><a href="' . wp_logout_url() .'">Logout</a></li>';
				} 
			?>
		</ul>

	</div><!--.wrap-->
</div><!--.woo-header-->