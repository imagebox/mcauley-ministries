<section class="four-photos">
	<img src="<?php the_sub_field('photo_one');?>" alt=""/>
	<img src="<?php the_sub_field('photo_two');?>" alt=""/>
	<img src="<?php the_sub_field('photo_three');?>" alt=""/>
	<img src="<?php the_sub_field('photo_four');?>" alt=""/>	
</section>