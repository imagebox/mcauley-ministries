
<section class="fullwidth-column section <?php if ( get_sub_field('include_sidebar_navigation') == 'yes' ) { echo 'has-sidebar'; } ?> <?php if( get_sub_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>

	<div class="wrap">
		<div class="column-content">

			<?php if ( get_sub_field( 'include_breadcrumbs' ) == 'yes' ) : ?>
				<?php get_template_part( 'inc/breadcrumbs' ); ?>
			<?php endif; ?>

			<?php the_sub_field('content'); ?>

			<?php if( get_sub_field('add_a_carousel') == 'yes' ) { ?>
				<div class="carousel-wrapper">
					
					<div id="inner-carousel" class="owl-carousel">
						<?php if( have_rows('carousel') ): while( have_rows('carousel') ): the_row(); ?>
						<div class="item <?php if(get_sub_field('link')) { ?>link<?php } else {?>no-link<?php } ?>">
							<?php if(get_sub_field('link')) { ?>
							<a href="<?php the_sub_field('link');?>" target="_blank"> 
							<?php } ?>		
								<img src="<?php the_sub_field('logo'); ?>" alt=""/>
							<?php if(get_sub_field('link')) { ?>
							</a>
							<?php } ?>		
						</div>
						<?php endwhile; endif; ?>
					</div><!-- .owl-carousel -->

				</div><!--.carousel-wrapper-->
			<?php } ?>

		</div>

		<?php if ( get_sub_field( 'include_sidebar_navigation' ) == 'yes' ) : ?>
			<?php include(locate_template('inc/sidebar-inner.php')); ?>
		<?php endif; ?>

	</div>
</section>
