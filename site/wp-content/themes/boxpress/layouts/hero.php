
<section class="hero <?php if( get_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_field('tiled_image') .') repeat;"';}?>>
	<div class="wrap">

		<?php

			$hero_image_left 	= get_field('hero_image_left');
			$hero_image_right = get_field('hero_image_right');
		?>
		
		<?php if ( ! empty( $hero_image_left )) : ?>

			<div class="hero-image__left">
				<img src="<?php echo $hero_image_left['url']; ?>" alt="">
			</div>

		<?php endif; ?>


		<div class="hero__body">
			<div class="hero-body-inner">
	
				<h1><?php the_field('hero_heading');?></h1>

				<?php if ( get_field('hero_subhead')) : ?>
				
					<h6><?php the_field('hero_subhead'); ?></h6>
				
				<?php endif; ?>

				<?php if ( get_field('hero_link')) : ?>
					
					<a class="button" href="<?php the_field('hero_link'); ?>">
						<?php the_field('hero_link_text'); ?>
					</a>

				<?php endif; ?>
				
			</div>
		</div>

		<?php if ( ! empty( $hero_image_right )) : ?>
			
			<div class="hero-image__right">
				<img src="<?php echo $hero_image_right['url']; ?>" alt="">
			</div>

		<?php endif; ?>


	</div><!--.wrap-->
</section><!--.hero-->
