<?php if( get_sub_field('overlay') == 'yes' ) { echo '<div class="overlay-wrap"><div class="overlay"></div>'; }?>

	<!--[if lte IE 8]>
	<section class="full-width parallax ie8plax" style="background-image: url(<?php the_sub_field('image'); ?>);">
	<![endif]-->	

	<!--[if IE 9]>
	<section class="full-width parallax" style="background-image: url(<?php the_sub_field('image'); ?>);">
	<![endif]-->

	<!--[if !IE]> -->
	<section class="full-width parallax" style="background-image: url(<?php the_sub_field('image'); ?>);">
	<!-- <![endif]-->	
		<div class="wrap">

			<?php if(get_sub_field('content')) { ?>
				<h4><?php the_sub_field('content'); ?></h4>
			<?php } ?>
			<?php if(get_sub_field('button_link')) { ?>
				<a href="<?php the_sub_field('button_link');?>" class="button white border"><?php the_sub_field('button_text');?></a>
			<?php } ?>

		</div>
	</section><!--.fullwidth alt-color -->

<?php if( get_sub_field('overlay') == 'yes' ) { echo '</div>'; }?>