<section class="split innerpage">
	<div class="wrap">
		<div class="content">
			<div class="inner">
				<?php if(get_sub_field('heading')) { ?>
					<h1><?php the_sub_field('heading'); ?></h1>
				<?php } ?>
				<?php if(get_sub_field('content')) { ?>
					<p><?php the_sub_field('content'); ?></p>
				<?php } ?>
			</div>
		</div><!--.content-->
		<div class="image">
			<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('heading');?>"/>
		</div>
	</div><!--.wrap-->
</section><!--.split-->