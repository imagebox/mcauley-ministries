<section class="full-width section <?php if( get_sub_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>
	<div class="wrap">

		<?php if(get_sub_field('heading')) { ?>
			<h2><?php the_sub_field('heading'); ?></h2>
		<?php } ?>
		
		<?php if( have_rows('two_columns') ): while ( have_rows('two_columns') ) : the_row();?>
		<div class="columns two">
			<div class="column-one column">
				<?php the_sub_field('column_one');?>
			</div>
			<div class="column-two column">
				<?php the_sub_field('column_two');?>
			</div>
		</div>	
		

		<?php endwhile; else : endif; ?>
						
	
	</div>
</section><!--.fullwidth alt-color -->