<section class="full-width section <?php if( get_sub_field('background') == 'color-option-1' ) { echo 'color-option-1'; }?><?php if( get_sub_field('background') == 'color-option-2' ) { echo 'color-option-2'; }?><?php if( get_sub_field('background') == 'color-option-3' ) { echo 'color-option-3'; }?><?php if( get_sub_field('background') == 'color-option-4' ) { echo 'color-option-4'; }?><?php if( get_sub_field('background') == 'color-option-5' ) { echo 'color-option-5'; }?>" <?php if( get_sub_field('background') == 'tiled-image' ) { echo 'style="background:url('. get_sub_field('tiled_image') .') repeat;"';}?>>
	<div class="wrap">
		
		<?php if(get_sub_field('heading')) { ?>
			<h2><?php the_sub_field('heading'); ?></h2>
		<?php } ?>
		
		<?php if( have_rows('columns') ): while ( have_rows('columns') ) : the_row();?>
			
			<div class="columns thirds">

				<?php if( get_sub_field('column_position') == 'one_left' ) {?> 
					<div class="column-one-third left column">
						<?php the_sub_field('one_third');?>
					</div>
					<div class="column-two-thirds column">
						<?php the_sub_field('two_thirds'); ?>
					</div>
				<?php } ?>
				
				<?php if( get_sub_field('column_position') == 'one_right' ) {?> 
					<div class="column-two-thirds column">
						<?php the_sub_field('two_thirds');?>
					</div>
					<div class="column-one-third right column">
						<?php the_sub_field('one_third'); ?>
					</div>
				<?php } ?>
		
			</div><!--.columns.thirds-->
		<?php endwhile; else : endif; ?>

	</div><!--.wrap-->
</section><!--.section-->