<?php 
/**
 * Template Name: Advanced
 */
get_header(); ?>

	<?php require_once('inc/banners/page-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

				<div class="entry-content">

				<?php

				/**
				 * Run clean shortcode for sub-fields.
				 * 
				 * Needs to be calld directly before sub-fields, won't
				 * work directly in functions.php because of how
				 * sub-fields are called.
				 */

				function boxpress_clean_shortcodes_acf( $content ) {   
				  
				  // els to remove
				  $array = array(
				    '<p>['    => '[',
				    ']</p>'   => ']',
				    '<div>['  => '[',
				    ']</div>' => ']',
				    ']<br />' => ']',
				    ']<br>'   => ']',
				    '<br />[' => '[',
				    '<br>['   => '[',
				  );

				  // Remove dem els
				  $content = strtr( $content, $array );
				  return $content;
				}
				add_filter('acf_the_content', 'boxpress_clean_shortcodes_acf');
				

				if( have_rows('innerpage_master') ): while ( have_rows('innerpage_master') ) : the_row(); ?>

					
					<?php if( get_row_layout() == 'full-width_column' ): ?>
			
						<?php include('layouts/full-width-column.php');?>
			
					<?php elseif( get_row_layout() == 'two_column_grid' ): ?>
						
						<?php include('layouts/two-column-grid.php');?>						
					
					<?php elseif( get_row_layout() == 'three_column_grid' ): ?>
							
						<?php include('layouts/three-column-grid.php');?>	

					<?php elseif( get_row_layout() == 'four_column_grid' ): ?>

						<?php include('layouts/four-column-grid.php');?>						

					<?php elseif( get_row_layout() == 'three_column_full_width' ): ?>
					
						<?php include('layouts/three-column-full-width.php');?>	

					<?php elseif( get_row_layout() == 'two-third_one-third_column' ): ?>
						
						<?php include('layouts/two-third-one-third-column.php');?>
					
					<?php elseif( get_row_layout() == 'parallax_background' ): ?>

						<?php include('layouts/parallax.php');?>

					<?php elseif( get_row_layout() == 'split_content_image' ): ?>						
						
						<?php include('layouts/split-content-image.php');?>						

					<?php elseif( get_row_layout() == 'team' ): ?>
						
						<?php include('layouts/team.php');?>						
					
					<?php elseif( get_row_layout() == 'logo_grid' ): ?>
						
						<?php include('layouts/logo-grid.php');?>	

					<?php elseif( get_row_layout() == 'slideshow' ): ?>

						<?php include('layouts/slideshow.php');?>

					<?php elseif( get_row_layout() == 'four_photos' ): ?>
						
						<?php include('layouts/four-photos.php');?>

					<?php elseif( get_row_layout() == 'large_hero_photo' ): ?>						
						
						<?php include('layouts/large-hero-photo.php');?>

					<?php elseif( get_row_layout() == 'google_map' ): ?>

						<?php include('layouts/google-map.php');?>

					<?php elseif( get_row_layout() == 'blog_posts' ): ?>

						<?php include('layouts/blog-posts.php');?>

					<?php elseif( get_row_layout() == 'gallery' ): ?>

						<?php include('layouts/gallery.php');?>


				<?php endif; endwhile; else : endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
