<?php 
/**
 * Template Name: Homepage
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main homepage" role="main">
			<section class="entry-content">
				<?php 
					if ( get_field('intro_type') == 'large-feature' ) { 
						include('layouts/hero.php');
					} else if ( get_field('intro_type') == 'slideshow' ) { 
						include('layouts/slideshow.php');
					}
				?>
			</section><!--.entry-content-->
			
			<?php
				$posts_count = count( get_field( 'featured_posts' ));
			?>
			<?php if ( have_rows( 'featured_posts' )) : ?>
				<section class="home-callout home-featured-news-block">
					<div class="wrap">
						<div class="l-grid-wrap">
							<div class="l-grid l-grid--<?php echo $posts_count ?>-col">
								<?php while ( have_rows( 'featured_posts' )) : the_row(); ?>
									<?php
										$featured_image = get_sub_field( 'featured_image' );
										$title 					= get_sub_field( 'title' );
										$description 		= get_sub_field( 'description' );
										$link_url 			= get_sub_field( 'link_url' );
									?>
									<?php if ( ! empty( $title ) && ! empty( $link_url )) : ?>
										<div class="l-grid-item">
											<a class="home-featured-post home-featured-post--<?php echo $posts_count; ?>"
												href="<?php echo esc_url( $link_url ); ?>">
												<?php if ( $featured_image ) : ?>
													<header class="featured-post-header">
														<img src="<?php echo esc_url( $featured_image['url'] ); ?>"
															width="<?php echo $featured_image['width']; ?>"
															height="<?php echo $featured_image['height']; ?>"
															alt="">
													</header>
												<?php endif; ?>
												<div class="featured-post-body">
													<div class="featured-post-content">
														<h3><?php echo $title; ?></h3>
														<?php if ( ! empty( $description )) : ?>
															<p><?php echo $description; ?></p>
														<?php endif; ?>
													</div>
												</div>
											</a>
										</div>
									<?php endif; ?>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				</section>
			<?php endif; ?>

			<?php get_template_part('template-parts/homepage-callout-1'); ?>

			<?php get_template_part('template-parts/homepage-callout-2'); ?>

			<div class="home-callout callout--slider color-option-4">
				<div class="wrap">
					<h2 class="h1">How We Help</h2>

					<?php if ( have_rows('carousel_callout') ) : ?>
						
						<div class="owl-carousel">

					  <?php while ( have_rows('carousel_callout') ) : the_row(); ?>
					  	<?php
					  		$carousel_callout_title 		= get_sub_field('carousel_callout_title');
					  		$carousel_callout_image 		= get_sub_field('carousel_callout_image');
					  		$carousel_callout_link_url 	= get_sub_field('carousel_callout_link_url');
					  	?>

					  	<?php if ( ! empty( $carousel_callout_image )) : ?>

								<div class="callout-disc">
									<a href="<?php echo $carousel_callout_link_url; ?>">
										
										<div class="circular-bkg">
											<div class="circular-image">
												<img src="<?php echo $carousel_callout_image['sizes']['home_carousel_thumb']; ?>" alt="">
											</div>
											
											<img class="circular-bkg--img" src="<?php bloginfo('template_directory'); ?>/assets/img/global/backgrounds/doodle-bkg.png" alt="">
										</div>

										<h5><?php echo $carousel_callout_title; ?></h5>
										
									</a>
								</div>

					  	<?php endif; ?>
					  <?php endwhile; ?>
						
						</div>

					<?php endif; ?>

				</div>
			</div>

			<?php get_template_part('template-parts/homepage-button-callout'); ?>

			<div class="home-callout callout--split">
				<div class="wrap">
					<header class="home-callout-header">
						<h2 class="h1">Grant Application Dates</h2>
					</header>
					<div class="split-1">
						<div class="callout--applications">

							<?php
								$major_grants_title = get_field( 'major_grants_title', 'option' );
							?>
							<?php if ( ! empty($major_grants_title) ) : ?>
								<h3><?php echo $major_grants_title; ?></h3>
							<?php endif; ?>

							<?php if ( have_rows('major_grants', 'option') ) : ?>
								<ul>
									<?php while ( have_rows('major_grants', 'option') ) : the_row(); ?>

										<?php
                      $date 						= get_sub_field('major_grant_date');
                      $is_closed 				= get_sub_field('is_major_grant_closed');
                      $grant_close_date = get_sub_field( 'major_grant_date' );

                      // Check dates
                      $now = wp_date( 'M j, Y' );
                      $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
                      $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
										?>
										
										<li>
											<div class="media-block media--date">
												<div class="media-figure">

													<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
														<h4>Closed</h4>
													<?php else : ?>
														<h4><?php echo date_month_formatter( $date ); ?></h4>
													<?php endif; ?>

												</div>
												<div class="media-body">
													<?php the_sub_field('major_grant_title'); ?>
												</div>
											</div>
										</li>

									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
					<div class="split-2">
						<div class="callout--applications">

							<?php
								$outreach_grants_title = get_field( 'outreach_grants_title', 'option' );
							?>
							<?php if ( ! empty($outreach_grants_title) ) : ?>
								<h3><?php echo $outreach_grants_title; ?></h3>
							<?php endif; ?>

							<?php if ( have_rows('outreach_grants', 'option') ) : ?>
								<ul>
									<?php while ( have_rows('outreach_grants', 'option') ) : the_row(); ?>

										<?php
                      $date 						= get_sub_field('outreach_grant_date');
                      $is_closed 				= get_sub_field('is_outreach_grant_closed');
                      $grant_close_date = get_sub_field( 'outreach_grant_date' );

                      // Check dates
                      $now = wp_date( 'M j, Y' );
                      $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
                      $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
										?>

										<li>
											<div class="media-block media--date">
												<div class="media-figure">
													<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
														<h4>Closed</h4>
													<?php else : ?>
														<h4><?php echo date_month_formatter( $date ); ?></h4>
													<?php endif; ?>
												</div>
												<div class="media-body">
													<?php the_sub_field('outreach_grant_grant_title'); ?>
												</div>
											</div>
										</li>

									<?php endwhile; ?>
								</ul>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
