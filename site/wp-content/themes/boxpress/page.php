<?php get_header(); ?>

	<?php require_once('inc/banners/page-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main simple" role="main">
				
				<div class="wrap">
					<div class="entry-content">
					
						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'page' ); ?>
	
						<?php endwhile; // end of the loop. ?>

					</div><!--.entry-content-->
					<?php get_sidebar(); ?>
				</div><!--.wrap-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
