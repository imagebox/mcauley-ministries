<aside class="sidebar" role="complementary">

	<?php
	// if not in the blog section
	if ( !is_single() && !is_home() && !is_archive()  ) : ?>	

		<?php /* Dish out a sidebar navigation for innerpages */
		global $wp_query;
		$post = $wp_query->post;
		$ancestors = get_post_ancestors($post);

		if ( empty($post->post_parent) ) {
		  $parent = $post->ID;

		} else {
		  $parent = end($ancestors);
		}

		if ( wp_list_pages("title_li=&child_of=$parent&echo=0" )) : ?>
			<div class="sidebar-nav">
				<ul>
				  <?php wp_list_pages("title_li=&child_of=$parent&depth=3" ); ?>
				</ul>
			</div><!-- /.sidebar-nav -->
		<?php endif; ?>

	<?php endif; ?>


	<?php
	// Show Categories & Archives in the blog only
 	if ( is_home() || is_single() || is_archive() ) : ?>

		<div class="sidebar-blog">
			<div class="widget">
				<h4>Categories</h4>
				
				<div class="sidebar-nav nav--cats">
					<ul>
				    <?php
					    // Show all cetegories and hide default 'Uncategorized'
					    wp_list_categories( array(
					      'title_li' 		=> '',
					      'hide_empty' 	=> false,
					      'exclude' 		=> 1,
					    ));
				    ?>
					</ul>
				</div>
			</div><!--.widget-->
		
			<div class="widget">
				<h4>Archives</h4>

				<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
					<option value=""><?php echo esc_attr( __( 'Select Month' )); ?></option> 
					<?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 )); ?>
				</select>
			</div><!--.widget-->
		</div>

		<!--
		<div class="widget tags">
			<h4>Tags</h4>
			<ul>
				<?php
					// $tags = get_tags( array(
					// 	'orderby' => 'count',
					// 	'order' => 'DESC'
					// ));

					// foreach ( (array) $tags as $tag ) {
					// 	echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">' . $tag->name . '  </a></li>';
					// }
				?>
			</ul>
			
		</div>
		-->
 
	<?php endif; ?>
	
	<?php // Only display on 'How to Apply' pages ?>
	<?php if ( is_page(184) || is_ancestor(184) ) : ?>
		
		<div class="sidebar-callout color-option-4">
			<div class="sidebar-callout-container">
				<h2>Grant Application Dates</h2>

				<?php
					$major_grants_title = get_field( 'major_grants_title', 'option' );
				?>
				<?php if ( ! empty($major_grants_title) ) : ?>
					<h3><?php echo $major_grants_title; ?></h3>
				<?php endif; ?>

				<?php if ( have_rows('major_grants', 'option') ) : ?>
					<ul class="grants-list">
						<?php while ( have_rows('major_grants', 'option') ) : the_row(); ?>
							<?php
	              $date 						= get_sub_field('major_grant_date');
	              $is_closed 				= get_sub_field('is_major_grant_closed');
	              $grant_close_date = get_sub_field( 'major_grant_date' );

	              // Check dates
	              $now = wp_date( 'M j, Y' );
	              $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
	              $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
							?>
							<li>
								<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
									<h4>Closed</h4>
								<?php else : ?>
									<h4><?php echo date_month_formatter( $date ); ?></h4>
								<?php endif; ?>
								<?php the_sub_field('major_grant_title'); ?>
							</li>

						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<hr>

				<?php
					$outreach_grants_title = get_field( 'outreach_grants_title', 'option' );
				?>
				<?php if ( ! empty($outreach_grants_title) ) : ?>
					<h3><?php echo $outreach_grants_title; ?></h3>
				<?php endif; ?>

				<?php if ( have_rows('outreach_grants', 'option') ) : ?>
					<ul class="grants-list">
						<?php while ( have_rows('outreach_grants', 'option') ) : the_row(); ?>
							<?php
                $date 						= get_sub_field('outreach_grant_date');
                $is_closed 				= get_sub_field('is_outreach_grant_closed');
                $grant_close_date = get_sub_field( 'outreach_grant_date' );

                // Check dates
                $now = wp_date( 'M j, Y' );
                $current_timestamp  = DateTime::createFromFormat( 'M j, Y', $now );
                $date_one_timestamp = DateTime::createFromFormat( 'M j, Y', $grant_close_date );
							?>
							<li>
								<?php if ( $current_timestamp > $date_one_timestamp || $is_closed ) : ?>
									<h4>Closed</h4>
								<?php else : ?>
									<h4><?php echo date_month_formatter( $date ); ?></h4>
								<?php endif; ?>
								<?php the_sub_field('outreach_grant_grant_title'); ?>
							</li>

						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

				<a href="<?php echo esc_url( site_url( '/how-to-apply/apply-online/' )); ?>" class="button">Apply For A Grant</a>
			</div>
		</div>

	<?php endif; ?>


	<div class="widget-area">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div>

</aside><!--.sidebar-->
