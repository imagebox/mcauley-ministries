<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('inc/banners/locations-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="fullwidth-column section">
				<div class="wrap">
					
					<div class="entry-content">

						<?php while ( have_posts() ) : the_post(); ?>

							<h1><?php the_title();?></h1>

							<?php the_content(); ?>
							
							<?php 
								$location = get_field('map');
								if( !empty($location) ):
							?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
							<?php endif; ?>


							


						<?php endwhile; // end of the loop. ?>

					</div><!-- .entry-content-->

			
				</div><!-- .wrap-->
			</section><!--.fullwidth-column.section-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>