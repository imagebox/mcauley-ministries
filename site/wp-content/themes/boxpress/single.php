<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('inc/banners/blog-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="blog-page">
				<div class="wrap">

					<div class="entry-content">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'content', 'single' ); ?>

							<?php // the_post_navigation(); ?>

						<?php endwhile; // end of the loop. ?>

					</div><!-- .entry-content-->

					<?php get_sidebar(); ?>
			
				</div><!-- .wrap-->
			</section><!--.blog-page-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>