<?php
/**
 * Button Callout
 */



?>

<?php if ( have_rows('button_callout') ) : ?>

  <div class="home-callout home-callout--buttons color-option-5">
    <div class="wrap">
      <ul>

        <?php while ( have_rows('button_callout') ) : the_row(); ?>
          <?php
            $button_callout_title = get_sub_field('button_callout_title');
            $button_callout_icon  = get_sub_field('button_callout_icon');
            $button_callout_url   = get_sub_field('button_callout_url');
          ?>

          <li>
            <a class="button has-media" href="<?php echo $button_callout_url; ?>">
              <div class="media-block is-reverse">
                <div class="media-figure">
                  <svg class="button-icon" width="21" height="21">
                    <use xlink:href="#<?php echo $button_callout_icon; ?>"></use>
                  </svg>
                </div>
                <div class="media-body">
                  <?php echo $button_callout_title; ?>
                </div>
              </div>
            </a>
          </li>

        <?php endwhile; ?>
        
      </ul>
    </div>
  </div>

<?php endif; ?>
