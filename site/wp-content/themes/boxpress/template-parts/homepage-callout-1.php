<?php
/**
 * Displays Homepage Callout 1
 */
$callout_1_heading      = get_field('callout_1_heading');
$callout_1_button_text  = get_field('callout_1_button_text');
$callout_1_button_url   = get_field('callout_1_button_url');
$callout_1_image        = get_field('callout_1_image');
?>

<div class="home-callout home-callout--1 color-option-2">
  <div class="wrap">
    <div class="callout-body">
      <h6><?php echo $callout_1_heading; ?></h6>

      <?php if ( ! empty( $callout_1_button_text ) && ! empty( $callout_1_button_url )) : ?>

        <a class="button" href="<?php echo $callout_1_button_url; ?>">
          <?php echo $callout_1_button_text; ?>
        </a>
        
      <?php endif; ?>

    </div>
    <div class="callout-figure">
    
      <?php if ( ! empty( $callout_1_image )) : ?>

        <img src="<?php echo $callout_1_image['url'] ?>" alt="">

      <?php endif; ?>
      
    </div>
  </div>
</div>
