<?php
/**
 * Displays Homepage Callout 2
 */
$callout_2_heading    = get_field('callout_2_heading');
$callout_2_subheading = get_field('callout_2_subheading');
?>

<div class="home-callout home-callout--2 color-option-3">
  <div class="wrap">
    <div class="callout-body">
      <h1><?php echo $callout_2_heading; ?></h1>

      <?php if ( ! empty( $callout_2_subheading )) : ?>

        <cite><?php echo $callout_2_subheading; ?></cite>
        
      <?php endif; ?>
    </div>
  </div>
</div>
