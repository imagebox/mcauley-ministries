<?php
/**
 * Displays the Post Preview template part
 *
 * @package BoxPress
 */

$sub_heading    = get_field('external_post_sub_heading');
$external_link  = get_field('external_post_link_url');
$is_external_link_disabled  = get_field('disable_external_link');
$link = get_permalink();
$target = '_self';

if ( ! $is_external_link_disabled && ! empty( $external_link )) {
  $link = $external_link;
  $target = '_blank';
}

$date = get_the_date('M j, Y');
?>

<article class="post-preview">
  <h2>
    <a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
      <?php the_title(); ?>
    </a>
  </h2>
  
  <?php if ( ! empty( $sub_heading )) : ?>
    <h3>
      <?php echo $sub_heading; ?>
    </h3>
  <?php endif; ?>

  <h4 class="post-date">
    <?php echo date_month_formatter( $date ); ?>
    <?php
      // if ( is_home() || is_category() ) {
        $categories_list = get_the_category_list( __( ', ', 'boxpress' ) );
        if ( $categories_list && boxpress_categorized_blog() ) {
          printf( ' | <span class="cat-links">' . __( '%1$s', 'boxpress' ) . '</span>', $categories_list );
        }
      // }
    ?>
  </h4>

  <?php the_excerpt(); ?>

</article>
