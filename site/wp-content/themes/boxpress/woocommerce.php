<?php get_header(); ?>

	<?php require_once('inc/banners/woo-banners.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main section" role="main">
			<div class="wrap">
				<div class="entry-content ">
					
					<?php woocommerce_content(); ?>

				</div><!--entry-content-->
				<?php get_sidebar('woo');?>
			</div><!--.wrap-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
